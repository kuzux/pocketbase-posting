migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("b9oqrscwiy6d5n5")

  collection.listRule = ""

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("b9oqrscwiy6d5n5")

  collection.listRule = null

  return dao.saveCollection(collection)
})
