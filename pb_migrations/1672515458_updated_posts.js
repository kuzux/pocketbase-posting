migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("b9oqrscwiy6d5n5")

  collection.createRule = "@request.auth.id = poster.id"
  collection.updateRule = "@request.auth.id = poster.id"

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("b9oqrscwiy6d5n5")

  collection.createRule = null
  collection.updateRule = null

  return dao.saveCollection(collection)
})
