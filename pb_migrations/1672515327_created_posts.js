migrate((db) => {
  const collection = new Collection({
    "id": "b9oqrscwiy6d5n5",
    "created": "2022-12-31 19:35:27.935Z",
    "updated": "2022-12-31 19:35:27.935Z",
    "name": "posts",
    "type": "base",
    "system": false,
    "schema": [
      {
        "system": false,
        "id": "yu9vv9f1",
        "name": "body",
        "type": "text",
        "required": true,
        "unique": false,
        "options": {
          "min": null,
          "max": null,
          "pattern": ""
        }
      }
    ],
    "listRule": null,
    "viewRule": null,
    "createRule": null,
    "updateRule": null,
    "deleteRule": null,
    "options": {}
  });

  return Dao(db).saveCollection(collection);
}, (db) => {
  const dao = new Dao(db);
  const collection = dao.findCollectionByNameOrId("b9oqrscwiy6d5n5");

  return dao.deleteCollection(collection);
})
