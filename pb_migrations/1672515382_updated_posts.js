migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("b9oqrscwiy6d5n5")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "6tbrb1pr",
    "name": "poster",
    "type": "relation",
    "required": true,
    "unique": false,
    "options": {
      "maxSelect": 1,
      "collectionId": "_pb_users_auth_",
      "cascadeDelete": false
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("b9oqrscwiy6d5n5")

  // remove
  collection.schema.removeField("6tbrb1pr")

  return dao.saveCollection(collection)
})
