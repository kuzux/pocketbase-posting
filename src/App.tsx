import {
    createBrowserRouter,
    RouterProvider,
} from "react-router-dom";

import Login from "./pages/login";
import Signup from "./pages/signup";
import Home from "./pages/home";
import Posts from "./pages/posts";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { PocketbaseProvider } from "./pocketbase";
import { ToastContainer } from "react-toastify";

import 'react-toastify/dist/ReactToastify.min.css';

const router = createBrowserRouter([
    {
        path: "/",
        element: <Home />,
    },
    {
        path: "/login",
        element: <Login />
    },
    {
        path: "/signup",
        element: <Signup />
    },
    {
        path: "/posts/:username",
        element: <Posts />
    }
]);

const queryClient = new QueryClient();

function App() {
    return <QueryClientProvider client={queryClient}>
        <PocketbaseProvider>
            <RouterProvider router={router} />
            <ToastContainer />
        </PocketbaseProvider>
    </QueryClientProvider>
}

export default App;
