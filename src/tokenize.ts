type TokenType = "text" | "at-mention";
type Token = {
    type: TokenType;
    body: string;
};

export function tokenize(post: string): Token[] {
    let tokens: Token[] = [];
    let previousToken = (): Token | undefined => {
        if(tokens.length === 0) return undefined;
        return tokens[tokens.length-1];
    }
    let inFirstLetter = true; // first letter of the new token
    for(let ch of post) {
        let isWhitespace = (ch == ' ') || (ch == '\t') || (ch == '\n');
        let prev = previousToken();

        if(isWhitespace) {
            inFirstLetter = true;
            if(prev === undefined || prev.type !== "text") {
                // we want space characters to always be part of text tokens
                // never links
                tokens.push({ type: "text", body: ch });
            } else {
                // prev was a text token
                prev.body += ch;
            }
            continue;
        }

        if(inFirstLetter) {
            if(ch === "@") {
                tokens.push({ type: "at-mention", body: "" });
            } else if(prev?.type === "text") {
                prev!.body += ch;
            } else {
                tokens.push({ type: "text", body: ch });
            }
            inFirstLetter = false;
            continue;
        }

        prev!.body += ch;
    }

    return tokens;
}