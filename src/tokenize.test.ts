import { tokenize } from "./tokenize";
import { test, expect } from "vitest";

test('tokenizer works correctly', () => {
    expect(tokenize("")).toEqual([]);
    expect(tokenize("asd qwe")).toEqual([{ type: "text", body: "asd qwe" }]);
    expect(tokenize("  asd qwe")).toEqual([{ type: "text", body: "  asd qwe" }]);
    expect(tokenize("asd @qwe zxc")).toEqual([
        { type: "text", body: "asd " },
        { type: "at-mention", body: "qwe"},
        { type: "text", body: " zxc" }
    ]);
});
