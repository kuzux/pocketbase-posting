import { useQuery } from "@tanstack/react-query";
import { useReducer } from "react";
import { Link } from "react-router-dom";
import { usePocketbase } from "../pocketbase";

function Following() {
    let pb = usePocketbase();

    let userId = pb.authStore.model?.id;
    type Post = {
        id: string;
        body: string;
        posterName: string;
    };
    let posts = useQuery(["followingPosts", userId], async () => {
        let records = await pb.collection("followers").getFullList(200, {
            filter: `follower = "${userId}"`
        });
        let exprs = records.map(r => `poster="${r.followed}"`);
        let filter = exprs.join("||");
        let postRecords = await pb.collection("posts").getFullList(200, {
            expand: "poster",
            sort: "-created",
            filter 
        });
        let posts: Post[] = postRecords.map(r => {
            let poster = (Array.isArray(r.expand.poster) ? r.expand.poster[0] : r.expand.poster);
            return { id: r.id, body: r.body, posterName: poster.username }
        });
        return posts;
    });

    if(posts.isLoading) {
        return <p>Loading...</p>;
    }

    let listItems = posts.data?.map(post => {
        console.log(post);
        return <li key={post.id}>
            <Link to={`/posts/${post.posterName}/`}>{post.posterName}</Link>: {post.body}
        </li>
    });

    return <ul>{listItems}</ul>;
}

function Home(): React.ReactElement | null {
    let [_rerenderCount, forceRerender] = useReducer((count) => count+1, 0);
    let pb = usePocketbase();

    if(pb.authStore.model === null) {
        return <div>
            <p>You are not logged in</p>
            <p><Link to="/signup">Signup</Link></p>
            <p><Link to="/login">Login</Link></p>
        </div>
    }

    let logoutClicked = () => {
        pb.authStore.clear();
        forceRerender();
    };

    return <div>
        <p>You are logged in as {pb.authStore.model.username}</p>
        <p><Link to={`/posts/${pb.authStore.model.username}`}>Go to my posts</Link></p>
        <p><button onClick={logoutClicked}>Logout</button></p>
        <Following />
    </div>
}

export default Home;