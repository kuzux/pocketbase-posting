import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { usePocketbase } from "../pocketbase";

function Signup(): React.ReactElement | null {
    let [email, setEmail] = useState("");
    let [username, setUsermame] = useState("");
    let [password, setPassword] = useState("");
    let navigate = useNavigate();
    let pb = usePocketbase();

    let signupClicked = async () => {
        try {
            await pb.collection("users").create({
                email,
                username,
                password,
                passwordConfirm: password
            });
        } catch(err) {
            console.error(err);
            let msg = (err as Error)?.message;
            toast.error(msg, { autoClose: false });
            return;
        }
        toast.info("Logged in successfully");
        navigate("/login");        
    }

    return <div>
        <h1>Signup</h1>
        <p>
            Email:
            <input type="text" value={email} onChange={e => setEmail(e.target.value)} />
        </p>
        <p>
            Username:
            <input type="text" value={username} onChange={e => setUsermame(e.target.value)} />
        </p>
        <p>
            Password:
            <input type="password" value={password} onChange={e => setPassword(e.target.value)} />
        </p>
        <p>
            <button onClick={signupClicked}>Signup</button>
        </p>
        <p>
            <Link to="/login">Login</Link>
        </p>
    </div>
}

export default Signup;