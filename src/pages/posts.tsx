import { useState } from "react";
import { Link, useParams } from "react-router-dom";
import { useQuery, useQueryClient } from "@tanstack/react-query";
import { usePocketbase } from "../pocketbase";
import { toast } from "react-toastify";
import { tokenize } from "../tokenize";

type Post = {
    id: string;
    created: string;
    body: string;
    poster: string;
};

function PostView(props: { post: Post }): React.ReactElement {
    let tokens = tokenize(props.post.body);
    let elements = tokens.map(token => {
        if(token.type === "text")
            return <span key={token.body}>{token.body}</span>
        else
            return <Link key={token.body} to={`/posts/${token.body}`}>@{token.body}</Link>
    });

    return <>{elements}</>;
}

function Posts(): React.ReactElement {
    let { username } = useParams();
    let [body, setBody] = useState("");

    let pb = usePocketbase();
    let client = useQueryClient();
    
    let user = useQuery(["user", username], 
        () => pb.collection("users").getFirstListItem(`username="${username}"`));
    let posts = useQuery(["posts", user.data?.id],
        () => pb.collection("posts").getFullList<Post>(200, {
            filter: `poster="${user.data?.id}"`,
            sort: '-created'
        }),
        { enabled: user.data?.id !== undefined });
    let followers = useQuery(["followers", user.data?.id], 
        async () => {
            let records = await pb.collection("followers").getFullList(200, {
                filter: `followed="${user.data?.id}"`});
            return new Set(records.map(r => r.follower));
        }, {
            enabled: !user.isLoading
        });

    let createPost = async () => {
        try {
            await pb.collection("posts").create({
                poster: user.data?.id,
                body
            });
        } catch(err) {
            console.error(err);
            let msg = (err as Error)?.message;
            toast.error(`Failed to post: ${msg}`, { autoClose: false });
            return;
        }
        toast.info("Posted!");
        setBody("");
        client.invalidateQueries(["posts", user.data?.id]);
    };

    let postForm = <></>;
    if(pb.authStore.model?.username === username) {
        postForm = <p>
            <input type="text" value={body} onChange={evt=>setBody(evt.target.value)} />
            <button onClick={createPost}>Post</button>
        </p>
    }

    let followUser = async () => {
        try {
            pb.collection("followers").create({
                follower: pb.authStore.model?.id,
                followed: user.data?.id
            });
        } catch(err) {
            console.error(err);
            let msg = (err as Error)?.message;
            toast.error(msg, { autoClose: false });
            return;
        }

        toast.info(`Followed ${user.data?.username}`);
        client.invalidateQueries(["followers", user.data?.id]);
    }

    let unfollowUser = async () => {
        let followerId = pb.authStore.model?.id;
        let followedId = user.data?.id;
        try {
            let record = await pb.collection("followers").getFirstListItem(`follower="${followerId}" && followed="${followedId}"`);
            await pb.collection("followers").delete(record.id);
        } catch(err) {
            console.error(err);
            let msg = (err as Error)?.message;
            toast.error(msg, { autoClose: false });
            return;
        }

        toast.info(`Unfollowed ${user.data?.username}`);
        client.invalidateQueries(["followers", user.data?.id]);
    }

    let followButton = <></>;
    if(pb.authStore.model !== null && pb.authStore.model.username !== username && followers.isSuccess) {
        let isFollowing = followers.data.has(pb.authStore.model.id);
        if(isFollowing)
            followButton = <button onClick={unfollowUser}>Unfollow</button>;
        else
            followButton = <button onClick={followUser}>Follow</button>;

    }

    let postItems = posts.data?.map(post => <li key={post.id}><PostView post={post} /></li>);
    let postsList = <ul>{postItems}</ul>;

    return <div>
        <h1>Posts for user {username}</h1>
        { followButton }
        { postForm }
        { postsList }
    </div>;
}

export default Posts;