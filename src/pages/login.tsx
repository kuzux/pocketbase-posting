import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { usePocketbase } from "../pocketbase";

function Login(): React.ReactElement | null {
    let [email, setEmail] = useState("");
    let [password, setPassword] = useState("");
    let navigate = useNavigate();
    let pb = usePocketbase();

    let loginClicked = async () => {
        try {
            await pb.collection("users").authWithPassword(
                email,
                password);
        } catch(err) {
            console.error(err);
            let msg = (err as Error)?.message;
            toast.error(msg, { autoClose: false });
            return;
        }
        toast.info("Logged in successfully");
        navigate("/");
    }

    return <div>
        <h1>Login</h1>
        <p>
            Email:
            <input type="text" value={email} onChange={e => setEmail(e.target.value)} />
        </p>
        <p>
            Password:
            <input type="password" value={password} onChange={e => setPassword(e.target.value)} />
        </p>
        <p>
            <button onClick={loginClicked}>Login</button>
        </p>
        <p>
            <Link to="/signup">Signup</Link>
        </p>
    </div>
}

export default Login;