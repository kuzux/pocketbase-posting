import React from "react";
import PocketBase from "pocketbase";

const pb = new PocketBase('http://127.0.0.1:8899');
let PbContext = React.createContext(pb);

export function PocketbaseProvider(props: React.PropsWithChildren<{}>) {
    return <PbContext.Provider value={pb}>
        {props.children}
    </PbContext.Provider>;
}

export function usePocketbase() {
    return React.useContext(PbContext);
}
